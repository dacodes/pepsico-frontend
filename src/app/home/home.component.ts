import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public gifs:any
  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    this.getGifs('accepted')
  }

  getGifs(filter) {
    this.apiService.getGifs(filter).subscribe((response:any) => {
      if (response) {
        this.gifs = response
        console.log(this.gifs)
      }
    }, error => {
      console.log(error)
    });
  }
}
