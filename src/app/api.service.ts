import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';

@Injectable()
export class ApiService {
  public env: any = environment;
  private url = this.env.api.url;

  constructor(private httpClient: HttpClient) {
  }

  checkAuth() {
    if (localStorage.getItem('pepsico.token')) {
      return true;
    }
    else {
      return false;
    }
  }

  // Inicio de sesión
  login(data: any) {
    return this.httpClient.post(this.url + '/auth/login', data)
      .pipe(map(response => {
        return response;
      }));
  }

  // Registro
  register(data: any) {
    return this.httpClient.post(this.url + '/users', data);
  }

  // Lista de usuarios
  getUsers() {
    return this.httpClient.get(this.url + '/users');
  }

  // Lista de gifs
  getGifs(value) {
    let filter = ''
    if (value == 'accepted') {
      filter = '?filter=accepted';
    }
    return this.httpClient.get(this.url + '/gifs' + filter);
  }

  // Registro de gif
  registerGif(data: any, image: File) {
    console.log(data)
    const formData = new FormData();
    formData.append('name', data.name);
    formData.append('gif', image);
    return this.httpClient.post(this.url + '/gifs', formData);
  }

  // Detalle de un gif
  getGif(id) {
    return this.httpClient.get(this.url + '/gifs');
  }

  // Aceptar gif
  toAcceptGif(data: any, value: boolean) {
    data.accepted = value;
    return this.httpClient.put(this.url + '/gifs/' + data.id, data);
  }
}
