import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpConfigInterceptor} from './interceptor/httpconfig.interceptor';
import {ErrorHandlerInterceptor} from './interceptor/error-handler.interceptor';
import {AuthGuard} from './auth.guard';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {GifsComponent} from './gifs/gifs.component';
import {RegisterComponent} from './register/register.component';
import {UsersComponent} from './users/users.component';
import {GifsRegisterComponent} from './gifs-register/gifs-register.component';
import {LightboxModule} from 'ngx-lightbox';
import {NgImageSliderModule} from 'ng-image-slider';
import {ApiService} from './api.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ShareButtonsModule} from '@ngx-share/buttons';
import {ShareButtonsConfig} from '@ngx-share/core';

const customConfig: ShareButtonsConfig = {
  include: ['facebook']
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    GifsComponent,
    RegisterComponent,
    UsersComponent,
    GifsRegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ShareButtonsModule.withConfig(
      customConfig
    ),
    LightboxModule,
    NgImageSliderModule,
    NgbModule

  ],
  providers: [
    ApiService,
    AuthGuard,
    {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
