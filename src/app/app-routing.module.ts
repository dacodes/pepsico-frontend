import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { GifsComponent } from './gifs/gifs.component';
import { UsersComponent } from './users/users.component';
import { GifsRegisterComponent } from './gifs-register/gifs-register.component';

const routes: Routes = [
  { path: '', redirectTo: 'gifs', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'users', component: UsersComponent },
  { path: '', component: HomeComponent },
  {
    path: 'gifs', canActivate: [AuthGuard],
    children: [
      { path: '', component: GifsComponent },
      { path: 'register', component: GifsRegisterComponent },
      { path: 'edit/{id}', component: GifsComponent },
    ],
  },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
