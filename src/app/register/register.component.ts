import {Component, OnInit} from '@angular/core';
import {ApiService} from 'src/app/api.service';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import Swal from 'sweetalert2'
import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  submitted = false
  registerForm: FormGroup
  user: any

  constructor(private apiService: ApiService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    if (this.apiService.checkAuth()) {
      this.router.navigate(['/gifs'])
    }
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    Swal.fire({
      title: '',
      text: '¿Deseas confirmar tu registro?',
      type: 'question',
      confirmButtonText: 'Si',
      showCancelButton: true,
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiService.register(this.registerForm.value)
          .subscribe((response: any) => {
            Swal.fire({
              toast: true,
              position: 'top-end',
              type: 'success',
              title: 'Registro exitoso',
              showConfirmButton: false,
              timer: 1500
            })
            this.router.navigate(['/login'])
          }, error => {
            console.log(error)
            Swal.fire({
              toast: true,
              position: 'top-end',
              type: 'warning',
              title: error.error.error.message,
              showConfirmButton: false,
              timer: 1500
            })
          });
      }
    })
  }
}
