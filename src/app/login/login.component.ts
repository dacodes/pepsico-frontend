import {Component, OnInit} from '@angular/core';
import {ApiService} from 'src/app/api.service';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import Swal from 'sweetalert2'
import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted = false
  loginForm: FormGroup
  user: any

  constructor(private apiService: ApiService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    if (this.apiService.checkAuth()) {
      this.router.navigate(['/gifs'])
    }
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.apiService.login(this.loginForm.value)
      .subscribe((response: any) => {
        this.submitted = false;
        localStorage.setItem('pepsico.token', response.token);
        Swal.fire({
          toast: true,
          position: 'top-end',
          type: 'success',
          title: 'Bienvenido ' + response.name,
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigate(['/gifs'])
      }, error => {
        Swal.fire({
          toast: true,
          position: 'top-end',
          type: 'warning',
          title: 'Usuario/Contraseña incorrectos.',
          showConfirmButton: false,
          timer: 1500
        })
      });
  }
}
