import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GifsRegisterComponent } from './gifs-register.component';

describe('GifsRegisterComponent', () => {
  let component: GifsRegisterComponent;
  let fixture: ComponentFixture<GifsRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GifsRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GifsRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
