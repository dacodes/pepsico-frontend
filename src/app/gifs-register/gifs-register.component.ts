import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-gifs-register',
  templateUrl: './gifs-register.component.html',
  styleUrls: ['./gifs-register.component.css']
})
export class GifsRegisterComponent implements OnInit {
  submitted = false
  registerForm: FormGroup
  selectedImage: File
  srcImage:any

  constructor(private apiService: ApiService, private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      image: ['', Validators.required]
    })
  }

  onFileChanged(event:any) {
    this.selectedImage = event.target.files[0]
    if (this.selectedImage) {
      let fileReader = new FileReader();
      fileReader.onload = (e:any) => {
        this.srcImage = e.target.result;
      }
      fileReader.readAsDataURL(this.selectedImage);
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    Swal.fire({
      title: '',
      text: '¿Registrar gif?',
      type: 'question',
      confirmButtonText: 'Si',
      showCancelButton: true,
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiService.registerGif(this.registerForm.value, this.selectedImage)
        .subscribe((response:any) => {
          Swal.fire({
            toast: true,
            position: 'top-end',
            type: 'success',
            title: 'Gif registrado correctament',
            showConfirmButton: false,
            timer: 1500
          })
          this.router.navigate(['/gifs'])
        }, error => {
          console.log(error)
          Swal.fire({
            toast: true,
            position: 'top-end',
            type: 'warning',
            title: error.error.error.message,
            showConfirmButton: false,
            timer: 1500
          })
        });
      }
    })
  }
}
