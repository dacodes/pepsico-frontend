import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from 'src/app/api.service';
import {Router} from '@angular/router';
import {environment} from 'src/environments/environment';
import Swal from 'sweetalert2'
import {Lightbox} from 'ngx-lightbox';
import {NgbTabset, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-gifs',
  templateUrl: './gifs.component.html',
  styleUrls: ['./gifs.component.css']
})
export class GifsComponent implements OnInit {
  public gifs: any
  menuActivos = false;
  menuTodos = false;
  private albums: Array<any> = [];
  slider: Array<any> = [];
  show: boolean = true;

  @ViewChild('tabs', {static: false})
  private tabs: NgbTabset;

  constructor(
    private apiService: ApiService,
    private ngbTabset: NgbTabsetModule,
    private router: Router,
    private lightBox: Lightbox) {
  }

  ngOnInit() {
    this.getGifs('accepted');
  }

  getGifs(filter) {
    if (filter == 'accepted') {
      this.menuActivos = true;
      this.menuTodos = false;
    } else {
      this.menuActivos = false;
      this.menuTodos = true;
    }

    this.apiService.getGifs(filter).subscribe((response: any) => {
      if (response) {
        this.gifs = response;
        this.createAlbum();
        this.createSlider();
        this.tabs.select('tabla');
      }
    }, error => {
      console.log(error);
    });
  }

  toAccept(gif, value) {
    if (this.apiService.checkAuth()) {
      this.apiService.toAcceptGif(gif, value)
        .subscribe((response: any) => {
          if (this.menuActivos) {
            this.getGifs('accepted');
          } else {
            this.getGifs('all');
          }
          let title = 'Gif aceptado';
          if (!value) {
            title = 'Gif rechazado';
          }
          Swal.fire({
            toast: true,
            position: 'top-end',
            type: 'success',
            title: title,
            showConfirmButton: false,
            timer: 1500
          })
        }, error => {
          console.log(error)
          Swal.fire({
            toast: true,
            position: 'top-end',
            type: 'warning',
            title: error.error.error.message,
            showConfirmButton: false,
            timer: 1500
          })
        });
    }
    else {
      this.router.navigate(['/login']);
    }
  }

  logout() {
    localStorage.removeItem('pepsico.token')
    this.router.navigate(['/login']);
  }

  open(index: number) {
    this.lightBox.open(this.albums, index);
  }

  showGrid(show: boolean) {
    this.show = show;
  }

  private createAlbum() {
    this.albums = []
    this.gifs.forEach(gif => {
      const album = {
        src: gif.url,
        caption: gif.name,
        thumb: gif.url
      };

      this.albums.push(album);
    });
  }

  private createSlider() {
    this.slider = [];
    this.gifs.forEach(gif => {
      const image = {
        image: gif.url,
        thumbImage: gif.url,
        alt: gif.name,
        title: gif.name
      };
      this.slider.push(image);
    });
  }

}
