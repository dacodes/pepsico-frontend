
# Pepsi Frontend
This is the frontend for Pepsi Test

# Requirements

 - [node.js]([https://nodejs.org/en/](https://nodejs.org/en/))
 - [npm package manager](https://www.npmjs.com/)
 - [Angular cli](https://angular.io/guide/setup-local)

# Installation

Clone the repository 

    git clone https://bitbucket.org/dacodes/pepsico-frontend

Install dependencies

    npm install

# Execute unit tests

Run unit test

    ng test

# Run

Start development server with the command 
	
	ng serve --open

> For **Production** run `ng build --prod` and copy the files from the `dist` folder.
